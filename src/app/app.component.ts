import {Component, ViewChild} from '@angular/core';
import {Platform, ToastController, MenuController, Nav, LoadingController, AlertController} from 'ionic-angular';
import {StatusBar, Splashscreen, SQLite} from 'ionic-native';
import {GlasstypePage} from "../pages/glasstype/glasstype";
import {ManufacturersPage} from "../pages/manufacturers/manufacturers";
import {HeaderPage} from "../pages/header/header";
import {HttpProvider} from "../providers/http-provider";
import {Network} from "@ionic-native/network";
import {AppPreferences} from "@ionic-native/app-preferences";


declare var navigator: any;
declare var Connection: any;

/*"proxies": [
 {
 "path": "/rahul",
 "proxyUrl": "http://iskstaging.com/trueview/api"
 }
 ]*/

@Component({
    templateUrl: 'app.html',
    providers: [HttpProvider]
})
export class MyApp {
    @ViewChild(Nav) nav;
    rootPage = HeaderPage;

    loading: any;
    posts: any;
    em: any;
    pd: any;
    pages: Array<{title: string, icon: any}>;
    public database: SQLite;
    lastSync: any;
    optionData: String;
  
    setFilterData: Array<{id: string,name: string,filter_options: string,is_deleted: string}>;
    filter_value: any;


    setManuProductData: Array<{id: string, manufacturer_img: String}>;
    setCatData: Array<{name: string, icon: any, id: string, modified: any, filter: string}>;
    // setProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, name: String, description: String, cover_photo: any}>;


    public getCatData: Array<{name: string, icon: any, id: string}>;
    public getOptionData: Array<{name: string, id: string}>;
    getProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: Array<{product_photo: string}>, options_id: Array<{options_id: string}>}>;


    constructor(platform: Platform, private appPreferences: AppPreferences, private toastCtrl: ToastController, private menuCtrl: MenuController, private httpProvider: HttpProvider, public loadingCtrl: LoadingController, private network: Network, private alertCtrl: AlertController) {



        platform.ready().then(() => {

            // StatusBar.styleDefault();
        //    StatusBar.styleLightContent();
            StatusBar.hide();

            Splashscreen.hide();

           // Or to get a key/value pair
            appPreferences.fetch('name').then((val) => {
                console.log('Your name is', val);
                if(val!="rahul"){
                    appPreferences.store("name","rahul");
                    console.log('LLL',"LLLL");
                    this.checkInternet();
                }
                else{
                    console.log('RRR',"RRR");
                    this.database = new SQLite();
                    this.database .openDatabase({
                        name: "data.db",
                        location: "default"
                    }).then(() => {

                    this.database.executeSql("SELECT * FROM category", []).then((data) => {
                        this.getCatData = [];
                        if (data.rows.length > 0) {
                            
                            // this.presentToast(data.rows.length, "");
                            for (var i = 0; i < data.rows.length; i++) {
                                this.getCatData.push({
                                    name: data.rows.item(i).name,
                                    icon: data.rows.item(i).icon,
                                    id: data.rows.item(i).id
                                });

                                this.lastSync = data.rows.item(i).modified;

                            }
							console.log("getCatData: ",this.getCatData);
                        }
                    }, (error) => {
                        console.log("ERROR: " + JSON.stringify(error));
                    });
                    }, (error) => {
                        //this.presentToast("5","");
                    });



                }
            });




        });

    }


    checkInternet() {

        var networkState = navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';
        if ('No network connection' == states[networkState]) {

            let alert = this.alertCtrl.create({
                title: "Connection Status",
                subTitle: states[networkState],
                enableBackdropDismiss: false,
                buttons: [{
                    text: 'Ok',
                    handler: () => {
                        //platform.exitApp();
                        // Platform.exitApp();
                        navigator.app.exitApp();
                    }
                }]
            });
            alert.present(alert);
        }
        else {
            this.database = new SQLite();
            this.database .openDatabase({
                name: "data.db",
                location: "default"
            }).then(() => {
                this.database .executeSql("DROP TABLE IF EXISTS category", {}).then((data) => {

                }, (error) => {
                    //this.presentToast("4","");
                });
                // console.log("TABLE CREATED: ", data);

                this.database .executeSql("CREATE TABLE IF NOT EXISTS category (name TEXT, icon TEXT, id TEXT, modified TEXT, filter TEXT)", {}).then((data) => {


                }, (error) => {
                    //this.presentToast("3","");
                });


            }, (error) => {
                //this.presentToast("5","");
            });



            this.loading = this.loadingCtrl.create({
                spinner: 'hide',
                content: `
       <div class="loading" >
        <img src="assets/img/loader.gif" alt=""  class="loading-image">
        <p   class="lod-p">Loading...</p>
    </div>`,
                cssClass: 'lod-p'


            });
            // this.pService.start();
            // loading.present();
            this.loading.present();
            this.getData();
        }

    }


    syncAll() {
		console.log('working');
        var networkState = navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';
        if ('No network connection' == states[networkState]) {

            let alert = this.alertCtrl.create({
                title: "Connection Status",
                subTitle: states[networkState],
                enableBackdropDismiss: false,
                buttons: [{
                    text: 'Ok',
                    handler: () => {

                        alert.dismiss();
                        //platform.exitApp();
                        // Platform.exitApp();
                        //navigator.app.exitApp();
                    }
                }]
            });
            alert.present(alert);
        }
        else {


            this.database .executeSql("DROP TABLE IF EXISTS category", {}).then((data) => {
                // console.log("TABLE CREATED: ", data);


            }, (error) => {
                //this.presentToast("4","");
            });

            this.database .executeSql("CREATE TABLE IF NOT EXISTS category (name TEXT, icon TEXT, id TEXT, modified TEXT, filter TEXT)", {}).then((data) => {


            }, (error) => {
                //this.presentToast("3","");
            });


            this.loading = this.loadingCtrl.create({
                spinner: 'hide',
                content: `<div class="loading" >
        <img src="assets/img/loader.gif" alt=""  class="loading-image">
        <p   class="lod-p">Loading new data...</p>`,

                cssClass: 'lod-p'

            });
            // this.pService.start();
            // loading.present();
            this.loading.present();
            this.getData();
        }

    }

    createTable(tableName: String) {

        this.database .executeSql("DROP TABLE IF EXISTS " + tableName, {}).then((data) => {


        }, (error) => {

        });


        this.database .executeSql("CREATE TABLE IF NOT EXISTS " + tableName + "(id TEXT, product_cat_id TEXT, manufacturer_id TEXT,manufacturer_name TEXT,manufacturer_logo TEXT,name TEXT,description TEXT,options TEXT,documentation TEXT, cover_photo TEXT,product_photo TEXT,options_id TEXT)", {}).then((data) => {


        }, (error) => {
            this.presentToast("1", "");
        });

        


    }

    createOptionTable() {

        this.database .executeSql("DROP TABLE IF EXISTS OptionData", {}).then((data) => {


        }, (error) => {

        });


        this.database .executeSql("CREATE TABLE IF NOT EXISTS OptionData(OptionData TEXT)", {}).then((data) => {


        }, (error) => {
            this.presentToast("1", "");
        });


        this.database .executeSql("DROP TABLE IF EXISTS Filter", {}).then((data) => {
            
            
                    }, (error) => {
            
                    });
            
            
                    this.database .executeSql("CREATE TABLE IF NOT EXISTS Filter(id TEXT, name TEXT, filter_options TEXT, is_deleted TEXT)", {}).then((data) => {
            
            
                    }, (error) => {
                        this.presentToast("1", "");
                    });

        


    }

    


    getData() {

        this.loading.present();
        this.httpProvider.getDynamicData().subscribe(
            data => {

                //this.pService.done();
                this.posts = data.ProductCat;

                this.setCatData = data.ProductCat;
                this.setManuProductData = data.Manufacturer;
                //this.setManuProductData = data.OptionData;

                this.optionData = data.OptionData;

                this.setFilterData = data.Filter;
                

                this.createOptionTable();

                this.database.executeSql("INSERT INTO OptionData (OptionData) VALUES (?)", [JSON.stringify(this.optionData)]).then((data) => {


                   // this.presentToast("data insert", "");
                }, (error) => {

                });

//rahull
                for (var i = 0; i < this.setFilterData.length; i++) {

                    if (this.setFilterData[i].is_deleted != "1" && this.setFilterData[i]['filter_options']!=undefined) {
                       
                        this.database.executeSql("INSERT INTO Filter (id,name,filter_options,is_deleted) VALUES (?,?,?,?)", [this.setFilterData[i].id,this.setFilterData[i].name,JSON.stringify(this.setFilterData[i].filter_options),this.setFilterData[i].is_deleted]).then((data) => {
                            
                            
                           // this.presentToast("data insert", "");    
                                }, (error) => {
                            
                       });

                    }

                }                    

               


                console.log("Success : " + this.posts);

                this.loading.dismiss();


                for (var i = 0; i < this.setCatData.length; i++) {

                    if (this.setCatData[i].id != "0") {
                        var a1;
                        try {

                            if (this.setCatData[i].name.match(' ')) {
                                let a = this.setCatData[i].name.indexOf(" ");
                                a1 = this.setCatData[i].name.substring(0, a);
                            }
                            else {
                                a1 = this.setCatData[i].name;
                            }
                        } catch (ex) {

                            console.log(ex.toString());

                        }


                        this.createTable(a1);

                    }
                    else if (this.setCatData[i].name == "Manufacturers") {

                        this.database .executeSql("DROP TABLE IF EXISTS Manufacturers", {}).then((data) => {


                        }, (error) => {

                        });

                        this.database .executeSql("CREATE TABLE IF NOT EXISTS Manufacturers (id TEXT, manufacturer_img TEXT)", {}).then((data) => {


                        }, (error) => {

                        });
                    }

                    this.lastSync = this.setCatData[i].modified;
                    this.database.executeSql("INSERT INTO category (name, icon, id, modified, filter) VALUES (?,?,?,?,?)", [this.setCatData[i].name, this.setCatData[i].icon, this.setCatData[i].id, this.setCatData[i].modified, this.setCatData[i].filter]).then((data) => {

                    }, (error) => {

                    });
                }


                for (var i = 0; i < this.setCatData.length; i++) {
                    var a1;
                    if (this.setCatData[i].id != "0") {
                        try {

                            if (this.setCatData[i].name.match(' ')) {
                                let a = this.setCatData[i].name.indexOf(" ");
                                a1 = this.setCatData[i].name.substring(0, a);
                            } else {
                                a1 = this.setCatData[i].name;
                            }
                        } catch (ex) {

                            console.log(ex.toString());

                        }


                        var u = data.Products[this.setCatData[i].name].Product.length;
                        this.getProductData = data.Products[this.setCatData[i].name].Product;
                        //  this.getOptionData = data.Products[this.setCatData[i].name].Product.options_id.materials;


                        for (var j = 0; j < u; j++) {
                            /* if(j==0)
                             console.log("IMAGEEEEE"+JSON.stringify(this.getProductData[j].cover_photo));
                             if(j==0)
                             console.log("IMAGEEEEE-ENCODE"+btoa(this.getProductData[j].cover_photo));*/

                            this.database.executeSql("INSERT INTO " + a1 + "(id,product_cat_id,manufacturer_id,manufacturer_name,manufacturer_logo,name,description,options,documentation,cover_photo,product_photo,options_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", [this.getProductData[j].id, this.getProductData[j].product_cat_id, this.getProductData[j].manufacturer_id
                                , this.getProductData[j].manufacturer_name, this.getProductData[j].manufacturer_logo, this.getProductData[j].name, this.getProductData[j].description, this.getProductData[j].options, this.getProductData[j].documentation, this.getProductData[j].cover_photo, JSON.stringify(this.getProductData[j].product_photo), JSON.stringify(this.getProductData[j].options_id)]).then((data) => {


                            }, (error) => {
                                console.log("ERROR: " + JSON.stringify(error.err));

                            });


                        }

                    }

                    else if (this.setCatData[i].name == "Manufacturers") {

                        for (var j = 0; j < this.setManuProductData.length; j++) {

                            this.database.executeSql("INSERT INTO Manufacturers (id,manufacturer_img) VALUES (?,?)", [this.setManuProductData[j].id, this.setManuProductData[j].manufacturer_img]).then((data) => {

                            }, (error) => {

                            });
                        }
                    }
                }


                this.refresh();


            },

            err => {
                console.error("Error : " + err);
            }
        );

    }

    public secondTimeLaunch(){

        this.refresh();
    }

    public refresh() {
        this.database.executeSql("SELECT * FROM category", []).then((data) => {
            this.getCatData = [];
            if (data.rows.length > 0) {
                console.log("DAAAA: " + JSON.stringify(data));
                // this.presentToast(data.rows.length, "");
                for (var i = 0; i < data.rows.length; i++) {
                    this.getCatData.push({
                        name: data.rows.item(i).name,
                        icon: data.rows.item(i).icon,
                        id: data.rows.item(i).id
                    });

                }
            }
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error));
        });
    }

    presentToast(em, pd) {
        let toast = this.toastCtrl.create({
            message: em + '' + pd,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');

        });

        toast.present();
    }

    homePage() {


        let toast = this.toastCtrl.create({
            message: 'User was added successfully',
            duration: 3000,
            position: 'top'
        });
        toast.present();
        this.menuCtrl.close();
        console.log('Click Lisener');
    }

    openWindowsAndDoors() {

      /*  console.log('Open Windows & Doors');
        this.menuCtrl.close();
        //this.navCtrl.push(WindowsPage)
        this.nav.push(WindowsPage, {
            param1: 'Windows & Doors', param2: '1'
        });*/
    }


    newMethod(name: String, icon: any, id: String, getProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, name: String, description: String, cover_photo: any}>) {


        let a = name.indexOf(" ");
        var a1 = name.substring(0, a);

        this.database.executeSql("SELECT filter FROM category where id=" + id, []).then((data) => {
            // this.getCatData = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {


                    this.filter_value = data.rows.item(i).filter;


                    if (name == "Home") {

                        //returns view controller obj
                        let view = this.nav.getActive();
                        //prints out component name as string
                        this.menuCtrl.close();

                        if (view.instance instanceof HeaderPage) {

                        }
                        else {
                            console.log("VIEWWWWW" + view.component.name);

                            this.nav.push(HeaderPage);
                        }
                    }
                    else if (name == "Manufacturers") {
                        // this.presentToast("Manufacturers", "");
                        console.log('Manufacturers');
                        this.menuCtrl.close();
                        //this.navCtrl.push(WindowsPage)
                        this.nav.push(ManufacturersPage, {
                            name: name, id: id, icon: icon, data: getProductData, filterValue: this.filter_value
                        });
                    }
                    else if (a1 == "Sync") {

                        this.menuCtrl.close();
                        //this.presentToast(name, "");
                        this.syncAll();
                    }
                    else {
                        //this.presentToast("value ", this.filter_value);

                        console.log('Open Windows & Doors');
                        this.menuCtrl.close();
                        //this.navCtrl.push(WindowsPage)
                        this.nav.push(GlasstypePage, {
                            name: name, id: id, icon: icon, data: getProductData, filterValue: this.filter_value
                        });
                    }

                    break;

                }
            }
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error));
        });


    }

    openShowers() {

       /* console.log('Open Windows & Doors');
        this.menuCtrl.close();
        //this.navCtrl.push(WindowsPage)
        this.nav.push(ShowersPage, {
            param1: 'Custom Showers', param2: '3'
        });*/
    }

    openGlassType() {

        console.log('Open Windows & Doors');
        this.menuCtrl.close();
        //this.navCtrl.push(WindowsPage)
        this.nav.push(GlasstypePage, {
            param1: 'Glass Types', param2: '2'
        });
    }

    openManufacturers() {

        console.log('Open Windows & Doors');
        this.menuCtrl.close();
        //this.navCtrl.push(WindowsPage)
        this.nav.push(ManufacturersPage, {
            param1: 'Windows & Doors', param2: '1'
        });
    }


}
