import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {HeaderPage} from "../pages/header/header";

import {GlasstypePage} from "../pages/glasstype/glasstype";
import {ManufacturersPage} from "../pages/manufacturers/manufacturers";

import {Network} from "@ionic-native/network";
import {DetailpagePage} from "../pages/detailpage/detailpage";
import {AccordionModule, CarouselModule, ProgressbarModule} from "ngx-bootstrap";
import {NgProgressModule} from "ngx-progressbar";
import {SwiperModule} from "angular2-swiper-wrapper";
import {PerfectScrollbarModule, PerfectScrollbarConfigInterface} from "ngx-perfect-scrollbar";

import {AppPreferences} from "@ionic-native/app-preferences";



const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};



@NgModule({
    declarations: [
        MyApp,
        HeaderPage,
        GlasstypePage,
        ManufacturersPage,
        DetailpagePage
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        AccordionModule.forRoot(),
        NgProgressModule,
        SwiperModule,
        CarouselModule.forRoot(),
        ProgressbarModule.forRoot(),
        PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG)


    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HeaderPage,
        GlasstypePage,
        ManufacturersPage,
        DetailpagePage
    ],
    providers: [
        Network,
        AppPreferences,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {



}
