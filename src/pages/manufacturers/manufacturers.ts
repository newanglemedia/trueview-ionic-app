import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {HttpProvider} from "../../providers/http-provider";
import {SQLite} from "ionic-native";
import {GlasstypePage} from "../glasstype/glasstype";

/*
 Generated class for the Manufacturers page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-manufacturers',
    templateUrl: 'manufacturers.html',
    providers: [HttpProvider]
})
export class ManufacturersPage {
    loading: any;
    posts: any;
    em: any;
    // em: any;
    flag: boolean;
    icon: any;
    pd: String;
    public getRelatedSearchData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>;

    public database: SQLite;
    public getCatData: Array<{name: string, icon: any, id: string}>;
    public getCatRealName: Array<{name: string}>;

    getProductData: Array<{id: string, manufacturer_img: String}>;
    getRelatedData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>;


    constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private httpProvider: HttpProvider, public loadingCtrl: LoadingController) {

        this.loading = this.loadingCtrl.create({
            content: `<ion-spinner ></ion-spinner>`
        });

        this.em = navParams.get('name');
        // this.refresh(this.em);
        this.pd = navParams.get('id');
        this.icon = navParams.get('icon');
        this.flag = false;
        this.refresh();


    }

    public refresh() {

        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {

            this.database.executeSql("SELECT * FROM Manufacturers", []).then((data) => {
                this.getProductData = [];
                if (data.rows.length > 0) {

                    for (var i = 0; i < data.rows.length; i++) {
                        this.getProductData.push({
                            id: data.rows.item(i).id,
                            manufacturer_img: data.rows.item(i).manufacturer_img
                        });

                        this.getRelatedIDS_data();

                        // this.presentToast(tableName);
                    }
                }
            }, (error) => {
                console.log("ERROR: " + JSON.stringify(error));
            });

        }, (error) => {
            console.error("Unable to execute sql", error);
        });


    }

    presentToast(em, pd) {
        let toast = this.toastCtrl.create({
            message: 'User was added successfully' + em + '' + pd,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');

        });

        toast.present();
    }

    searchResult(name: String) {


        var k1 = 0;
        var k11 = 0;

        var number = 0;


        this.getRelatedSearchData = [];
        for (var i = 0; i < this.getCatData.length; i++) {
            //   k11 = i;
            let length = this.getCatData.length;
            let cat_id = this.getCatData[i].id;

            if (cat_id != "0") {
                let cat_name;
                if (this.getCatData[i].name.match(' ')) {
                    let a = this.getCatData[i].name.indexOf(" ");
                    cat_name = this.getCatData[i].name.substring(0, a);
                }
                else {
                    cat_name = this.getCatData[i].name;
                }

                console.log("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"');

                this.database.executeSql("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name LIKE "%' + name + '%"', []).then((data) => {


                    if (data.rows.length > 0) {
                        number = number + data.rows.length;
                        // this.presentToast(this.la);
                        k1 = number;
                        k11 = number;
                        //   this.presentToast(k1);
                        for (var k = 0; k < data.rows.length; k++) {

                            this.getRelatedSearchData.push({
                                id: data.rows.item(k).id,
                                product_cat_id: data.rows.item(k).product_cat_id,
                                manufacturer_id: data.rows.item(k).manufacturer_id,
                                manufacturer_name: data.rows.item(k).manufacturer_name,
                                manufacturer_logo: data.rows.item(k).manufacturer_logo,
                                name: data.rows.item(k).name,
                                description: data.rows.item(k).description,
                                options: data.rows.item(k).options,
                                documentation: data.rows.item(k).documentation,
                                cover_photo: data.rows.item(k).cover_photo,
                                product_photo: JSON.parse(data.rows.item(k).product_photo),
                                options_id: JSON.parse(data.rows.item(k).options_id)

                            });


                        }
                        // k1 = k1+k+1;

                        /* if(i==(this.getCatData.length-1)){
                         console.log('Open Windows & Doors');
                         this.presentToast(k1);

                         this.navCtrl.push(GlasstypePage, {
                         name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
                         });
                         }*/


                    }
                    else {
                        /* if (i == (this.getCatData.length - 1)) {
                         console.log('Open Windows & Doors');
                         this.presentToast(k1);

                         this.navCtrl.push(GlasstypePage, {
                         name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
                         });
                         }*/
                    }


                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });

            }


        }

        console.log('Open Windows & Doors');
       // this.presentToast(k1);

        this.navCtrl.push(GlasstypePage, {
            name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
        });


    }

    showSearch(name: String) {

        // this.presentToast("sads","");

        // if (name != ' ') {

        //  this.presentToast(name, "");
        this.flag = !this.flag;


        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {
            this.database.executeSql("SELECT * FROM category", []).then((data) => {
                this.getCatData = [];
                if (data.rows.length > 0) {
                    // this.presentToast(data.rows.length, "");
                    for (var i = 0; i < data.rows.length; i++) {


                        // this.presentToast(name, "");


                        this.getCatData.push({
                            name: data.rows.item(i).name,
                            icon: data.rows.item(i).icon,
                            id: data.rows.item(i).id
                        });


                    }
                }
            }, (error) => {
                console.log("ERROR: " + JSON.stringify(error));
            });
        }, (error) => {
            console.error("Unable to execute sql", error);
        });

        /*  }
         else {

         this.flag = !this.flag;
         }*/


    }

    getData() {
        this.loading.present();
        this.httpProvider.getManufacturerData().subscribe(
            data => {

                this.posts = data;
                console.log("Success : " + this.posts);
               // this.presentToast(this.posts, this.pd);
                //this.loading.dismiss();

            },

            err => {
                console.error("Error : " + err);
            },

            () => {
                this.loading.dismiss();
                console.log('getData completed');
                console.log("Success : " + this.posts);
              //  this.presentToast(this.posts, this.pd);
            }
        );

    }

    public getRelatedIDS_data() {
        this.database.executeSql("SELECT * FROM category", []).then((data) => {
            this.getCatData = [];
            if (data.rows.length > 0) {
                // this.presentToast(data.rows.length, "");
                for (var i = 0; i < data.rows.length; i++) {
                    this.getCatData.push({
                        name: data.rows.item(i).name,
                        icon: data.rows.item(i).icon,
                        id: data.rows.item(i).id
                    });

                }
            }
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error));
        });
    }

    listViewClick(index: string, icon:string) {

        this.getRelatedData = [];
        this.getCatRealName = [];
        let id = this.getProductData[index].id;

        for (var k = 0; k < this.getCatData.length; k++) {

            let cat_id = this.getCatData[k].id;

            if (cat_id != "0") {
                let n =this.getCatData[k].name;
                let cat_name;
                if(this.getCatData[k].name.match(' ')){
                    let a = this.getCatData[k].name.indexOf(" ");
                    cat_name = this.getCatData[k].name.substring(0, a);
                }
                else{
                    cat_name=this.em;
                }



                this.database.executeSql("SELECT * FROM " + cat_name, []).then((data) => {


                    if (data.rows.length > 0) {

                        for (var i = 0; i < data.rows.length; i++) {

                            if(data.rows.item(i).manufacturer_id==id){

                                this.getRelatedData.push({
                                    id: data.rows.item(i).id,
                                    product_cat_id: data.rows.item(i).product_cat_id,
                                    manufacturer_id: data.rows.item(i).manufacturer_id,
                                    manufacturer_name: data.rows.item(i).manufacturer_name,
                                    manufacturer_logo: data.rows.item(i).manufacturer_logo,
                                    name: data.rows.item(i).name,
                                    description: data.rows.item(i).description,
                                    options: data.rows.item(i).options,
                                    documentation: data.rows.item(i).documentation,
                                    cover_photo: data.rows.item(i).cover_photo,
                                    product_photo:JSON.parse(data.rows.item(i).product_photo),
                                    options_id: JSON.parse(data.rows.item(i).options_id)

                                });

                                this.getCatRealName.push({
                                    name:n
                                });
                            }


                        }
                    }
                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });

            }

        }




        console.log('Open Windows & Doors');

        this.navCtrl.push(GlasstypePage, {
            name: this.em, id: this.pd, icon: this.icon,data:this.getRelatedData,manuf_icon:icon,RealName:this.getCatRealName
        });

    }

    back() {

        this.navCtrl.pop();
    }
}