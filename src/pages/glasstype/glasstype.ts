import {Component, ViewChild, OnInit} from '@angular/core';
import {
    NavController, NavParams, ToastController, LoadingController, MenuController, Nav,
    AlertController
} from 'ionic-angular';
import {HttpProvider} from "../../providers/http-provider";
import {SQLite} from "ionic-native";
import {DetailpagePage} from "../detailpage/detailpage";
import {AppPreferences} from "@ionic-native/app-preferences";

/*
 Generated class for the Glasstype page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    templateUrl: 'glasstype.html',
    providers: [HttpProvider]
})
export class GlasstypePage {


    @ViewChild(Nav) nav;


    config: Object = {
        direction: 'horizontal',
        nextButton: '.swiper-button-next',
        pagination: '.swiper-pagination',
        prevButton: '.swiper-button-prev',
        slidesPerView: 'auto',
        keyboardControl: true
    };
    getKey: Array<{key: string}>;
    KeyForTable: any[];

    checkboxFlag: any[];
    flag1: boolean;
    loading: any;
    posts: any;
    em: any;
    pd: String;
    manuf_icon: String;
    filterValue: String;
    icon: any;
    public database: SQLite;
    totalResult: any;
    GlobsalTableName: String;
    flag: boolean;
    lenght: any;
    whichFilter: Array<{name: string}>;
    product_photo1: Array<{product_photo: any}>;
    // data: Array<{id: string, product_cat_id: String, manufacturer_id: string, name: String, description: String, cover_photo: any}>;
    public getCatRealName: Array<{name: string}>;

    getProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>;
    product_ids: Array<{id: string}>;
    sendProductData: any[];
    optionData: any[];
    getoptionData: Array<{id: String, name: String, image: String, color_code: String}>;

    getoptionDatakey: {};

    allIDS: any[];
    filter_flag: boolean;

    public getCatData: Array<{name: String, icon: any, id: String}>;
    appPreferences: AppPreferences;
    manuf_flag: boolean = false;
    result_flag: boolean = true;
    public getRelatedSearchData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>;
    product_ids_for_filters: any[];
    /*
     public la: number = 0;
     */
    getproduct_ids_for_filters: any[];

    alert: any;


    constructor(public navCtrl: NavController, public appPreferences1: AppPreferences, private menuCtrl: MenuController, public navParams: NavParams, private toastCtrl: ToastController, private httpProvider: HttpProvider, public loadingCtrl: LoadingController, private alertCtrl: AlertController) {

        this.loading = this.loadingCtrl.create({
            content: `<ion-spinner ></ion-spinner>`
        });

        this.alert = this.alertCtrl.create({
            title: "Content loading...",
            enableBackdropDismiss: false
        });
        this.alert.present(alert);

        this.appPreferences = appPreferences1;

        // this.checkboxFlag=false;
        this.flag = false;
        this.getKey = [];
        this.whichFilter = [];
        this.getoptionData = [];
        this.getoptionDatakey = [];
        this.getCatRealName = [];
        this.allIDS = [];
        this.getCatData = [];

        this.KeyForTable = [];

        this.flag1 = false;
        this.em = navParams.get('name');
        // this.refresh(this.em);

        this.pd = navParams.get('id');
        // this.presentToast("mydata" + this.pd);
        this.icon = navParams.get('icon');
		console.log('icon', this.icon);
        this.filterValue = navParams.get('filterValue');
        if (this.filterValue == "1") {
            this.filter_flag = true;
        }
        else {
            this.filter_flag = false;
        }
//this.presentToast(this.filter_flag+"")
        // this.data = navParams.get('data');

        // this.getData();

        let a = this.em.indexOf(" ");
        var a1 = this.em.substring(0, a);
        // this.refresh(a1);
        // this.presentToast(a1);

        var x = +this.pd;


        if (this.em == 'Manufacturers') {


            this.getProductData = navParams.get('data');
            this.manuf_icon = navParams.get('manuf_icon');
            this.manuf_flag = true;
            this.result_flag = false;
            this.getCatRealName = navParams.get("RealName");
            this.alert.dismiss();
        }
        else if (this.em.match('Search')) {

            this.getProductData = navParams.get('data');
           // this.presentToast("console:-" + x);
            this.totalResult = x;
            this.result_flag = false;
            this.alert.dismiss();
            //  this.la=0;
            /*for(var i=0;i<this.getProductData.length;i++){
             this.presentToast("console:-" + i);
             }*/


            /* this.appPreferences.fetch('result').then((val) => {

             this.totalResult = val;
             this.presentToast("console:-" + val);
             });*/


            //  this.setTotalReult(this.getProductData);

        }
        else {
            if (x != 0) {
                //this.presentToast(this.pd);
                var a1;
                if (this.em.match(' ')) {
                    let a = this.em.indexOf(" ");
                    a1 = this.em.substring(0, a);
                }
                else {
                    a1 = this.em;
                }


                this.refresh(a1);

            }
        }


    }

    public setTotalReult(getProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>) {
        // this.totalResult = this.pd;
       // this.presentToast("console:-" + getProductData.length);
    }

    public refresh(tableName: String) {


        this.GlobsalTableName = tableName;

        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {

            this.database.executeSql("SELECT * FROM " + tableName + " ORDER BY name", []).then((data) => {
                this.getProductData = [];
                this.product_ids = [];
                if (data.rows.length > 0) {
                    this.totalResult = data.rows.length;
                    for (var i = 0; i < data.rows.length; i++) {

                        /*  if(i==0)
                         console.log("IMAGEEEEE"+JSON.stringify(data.rows.item(i).cover_photo));*/

                        this.getProductData.push({
                            id: data.rows.item(i).id,
                            product_cat_id: data.rows.item(i).product_cat_id,
                            manufacturer_id: data.rows.item(i).manufacturer_id,
                            manufacturer_name: data.rows.item(i).manufacturer_name,
                            manufacturer_logo: data.rows.item(i).manufacturer_logo,
                            name: data.rows.item(i).name,
                            description: data.rows.item(i).description,
                            options: data.rows.item(i).options,
                            documentation: data.rows.item(i).documentation,
                            cover_photo: data.rows.item(i).cover_photo,
                            product_photo: JSON.parse(data.rows.item(i).product_photo),
                            options_id: JSON.parse(data.rows.item(i).options_id)

                        });

                        if (i == data.rows.length - 1) {
                            this.alert.dismiss();
                        }
                        /* if(i==0)
                         console.log("IMAGEEEEE-DECODE"+JSON.stringify(atob(data.rows.item(i).cover_photo)));*/

                        // this.presentToast(JSON.parse(data.rows.item(i).product_photo));
                        this.product_ids.push({id: data.rows.item(i).id});

                        //  this.product_photo1.push({product_photo:data.rows.item(i).product_photo});

                        //  this.presentToast(tableName);
                    }
                    this.getFilterData();
                   
                }
            }, (error) => {
                console.log("ERROR:----" + JSON.stringify(error));
            });

        }, (error) => {
            console.error("Unable to execute sql", error);
        });


    }

    searchResult(name: String) {


        var k1 = 0;
        var k11 = 0;

        var number = 0;


        this.getRelatedSearchData = [];
        for (var i = 0; i < this.getCatData.length; i++) {
            //   k11 = i;
            let length = this.getCatData.length;
            let cat_id = this.getCatData[i].id;

            if (cat_id != "0") {
                let cat_name;
                if (this.getCatData[i].name.match(' ')) {
                    let a = this.getCatData[i].name.indexOf(" ");
                    cat_name = this.getCatData[i].name.substring(0, a);
                }
                else {
                    cat_name = this.getCatData[i].name;
                }

                console.log("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"');

                this.database.executeSql("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name LIKE "%' + name + '%"', []).then((data) => {


                    if (data.rows.length > 0) {
                        number = number + data.rows.length;
                        // this.presentToast(this.la);
                        k1 = number;
                        k11 = number;
                        //   this.presentToast(k1);
                        for (var k = 0; k < data.rows.length; k++) {

                            this.getRelatedSearchData.push({
                                id: data.rows.item(k).id,
                                product_cat_id: data.rows.item(k).product_cat_id,
                                manufacturer_id: data.rows.item(k).manufacturer_id,
                                manufacturer_name: data.rows.item(k).manufacturer_name,
                                manufacturer_logo: data.rows.item(k).manufacturer_logo,
                                name: data.rows.item(k).name,
                                description: data.rows.item(k).description,
                                options: data.rows.item(k).options,
                                documentation: data.rows.item(k).documentation,
                                cover_photo: data.rows.item(k).cover_photo,
                                product_photo: JSON.parse(data.rows.item(k).product_photo),
                                options_id: JSON.parse(data.rows.item(k).options_id)

                            });


                        }
                        // k1 = k1+k+1;

                        /* if(i==(this.getCatData.length-1)){
                         console.log('Open Windows & Doors');
                         this.presentToast(k1);

                         this.navCtrl.push(GlasstypePage, {
                         name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
                         });
                         }*/


                    }
                    else {
                        /* if (i == (this.getCatData.length - 1)) {
                         console.log('Open Windows & Doors');
                         this.presentToast(k1);

                         this.navCtrl.push(GlasstypePage, {
                         name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
                         });
                         }*/
                    }


                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });

            }


        }

        console.log('Open Windows & Doors');
        //this.presentToast(k1);

        this.navCtrl.push(GlasstypePage, {
            name: 'Search for: ' + name, id: k11, icon: 'dsfdsf', data: this.getRelatedSearchData
        });


    }


    showSearch(name: String) {

        // this.presentToast("sads","");

        // if (name != ' ') {

        //  this.presentToast(name, "");
        this.flag = !this.flag;


        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {
            this.database.executeSql("SELECT * FROM category", []).then((data) => {
                this.getCatData = [];
                if (data.rows.length > 0) {
                    // this.presentToast(data.rows.length, "");
                    for (var i = 0; i < data.rows.length; i++) {


                        // this.presentToast(name, "");


                        this.getCatData.push({
                            name: data.rows.item(i).name,
                            icon: data.rows.item(i).icon,
                            id: data.rows.item(i).id
                        });


                    }
                }
            }, (error) => {
                console.log("ERROR: " + JSON.stringify(error));
            });
        }, (error) => {
            console.error("Unable to execute sql", error);
        });

        /*  }
         else {

         this.flag = !this.flag;
         }*/


    }

    public getFilterData() {

        // rahul sonkhiya
        this.database.executeSql("SELECT * FROM Filter", []).then((data) => {

            if (data.rows.length > 0) {

                for (var i = 0; i < data.rows.length; i++) {

                    var raw = JSON.parse(data.rows.item(i).filter_options);

                    for(var j=0;j<raw.length;j++){

                        if(this.pd==raw[j]){

                            this.whichFilter.push({name: data.rows.item(i).name});
                        }
                        
                    }

                }
                this.getOptionData();

            }

    }, (error) => {
        console.log("ERROR:[[[[[]]]]]]" + JSON.stringify(error));
    });
    }


    public getOptionData() {


        this.database.executeSql("SELECT * FROM OptionData", []).then((data) => {

            if (data.rows.length > 0) {

                //  this.presentToast(data.rows.length);

                for (var i = 0; i < data.rows.length; i++) {

                    var data1 = JSON.parse(data.rows.item(i).OptionData);

                    /*this.presentToast("fhgf"+i);

                     console.log("data1   " + data1);*/

                }

                

                for (var key in data1) {
                    for(var name =0;name<this.whichFilter.length;name++){
 
                        if(key.match(this.whichFilter[name].name)){
                            var a1;
                            this.getKey.push({key: key});
                           
        
                            if (key.match(' ')) {
                                let a = key.indexOf(" ");
                                a1 = key.substring(0, a);
                            } else {
                                a1 = key;
                            }
                            this.createTable(a1);
                        }
                    }
                    


                }

                for (var i = 0; i < this.getKey.length; i++) {

                    this.getoptionData = data1[this.getKey[i].key].Option;

                    // this.getoptionDatakey[this.getKey[i].key];
                    this.getoptionDatakey[this.getKey[i].key] = this.getoptionData;

                    if (this.getKey[i].key.match(' ')) {
                        let a = this.getKey[i].key.indexOf(" ");
                        a1 = this.getKey[i].key.substring(0, a);
                    } else {
                        a1 = this.getKey[i].key;
                    }
                  
                    for (var j = 0; j < this.getoptionData.length; j++) {

                        //  this.presentToast(this.getoptionData.length);
                        this.database.executeSql("INSERT INTO " + a1 + "(id,name,image,color_code) VALUES (?,?,?,?)", [this.getoptionData[j].id, this.getoptionData[j].name, this.getoptionData[j].image
                            , this.getoptionData[j].color_code]).then((data) => {


                        }, (error) => {
                            console.log("ERROR: " + JSON.stringify(error.err));

                        });
                    }


                }

                //  console.log("data1   " + data1);

            }
        }, (error) => {
            console.log("ERROR:[[[[[]]]]]]" + JSON.stringify(error));
        });

    }

    createTable(tableName: String) {

        this.database .executeSql("DROP TABLE IF EXISTS " + tableName, {}).then((data) => {


        }, (error) => {

        });


        this.database .executeSql("CREATE TABLE IF NOT EXISTS " + tableName + "(id TEXT, name TEXT, image TEXT,color_code TEXT)", {}).then((data) => {


        }, (error) => {

        });


    }

    createTableFilter() {

        this.database .executeSql("DROP TABLE IF EXISTS filter_name", {}).then((data) => {


        }, (error) => {

        });


        this.database .executeSql("CREATE TABLE IF NOT EXISTS filter_name(product_id TEXT, filter_id TEXT, name TEXT)", {}).then((data) => {

            console.log("TABLE CREATED");

        }, (error) => {

        });


    }

    presentToast(pd) {
        let toast = this.toastCtrl.create({
            message: pd,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');

        });

        toast.present();
    }

    getData() {
        this.loading.present();
        this.httpProvider.getJsonData(this.pd).subscribe(
            data => {

                this.posts = data;
                console.log("Success : " + this.posts);
                //this.presentToast(this.pd);
                //this.loading.dismiss();

            },

            err => {
                console.error("Error : " + err);
            },

            () => {
                this.loading.dismiss();
                console.log('getData completed');
                console.log("Success : " + this.posts);
               // this.presentToast(this.pd);
            }
        );

    }

    insertInFilter(product_id: String, filter_id: String, name: String) {

        this.database.executeSql("INSERT INTO filter_name(product_id,filter_id,name) VALUES (?,?,?)", [product_id, filter_id, name]).then((data) => {

            console.log("TABLE UPDATED " + "product id:- " + product_id + " filter id:- " + filter_id + " name:- " + name);


        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error.err));

        });
    }

    showForm() {

        this.flag1 = !this.flag1;

        if (this.allIDS.length > 0) {
            if (this.flag1 == false) {
               // this.presentToast("yes");
                this.createTableFilter();

                this.database.executeSql("SELECT * FROM " + this.GlobsalTableName, []).then((data) => {
                   // this.presentToast("I am in");
                    this.getProductData = [];
                    this.product_ids = [];
                    if (data.rows.length > 0) {
                        this.totalResult = data.rows.length;
                        for (var i = 0; i < data.rows.length; i++) {

                            this.optionData = JSON.parse(data.rows.item(i).options_id);

                            var id = data.rows.item(i).id;

                            for (var key in this.optionData) {

                                console.log("KEEEEE   " + key);
                                // var obj = this.options_id[key];

                                if (this.optionData[key].length != 0) {
                                    // this.getKey.push({key: key});

                                    for (var j = 0; j < this.optionData[key].length; j++) {

                                        for (var k = 0; k < this.allIDS.length; k++) {


                                            if (this.optionData[key][j].id == this.allIDS[k]) {

                                               // this.presentToast("I am here");
                                                this.insertInFilter(id, this.allIDS[k], key);


                                                break;
                                            }

                                            else {

                                            }

                                        }


                                    }
                                }


                            }


                            this.product_ids.push({id: data.rows.item(i).id});


                        }
                        this.getproduct_ids_for_filters = [];
                        for (var r = 0; r < this.KeyForTable.length; r++) {

                            var query = "SELECT product_id from filter_name where name='" + this.KeyForTable[r].toLowerCase() + "'";
                            console.log(query);

                            this.database.executeSql("SELECT product_id from filter_name where name='" + this.KeyForTable[r].toLowerCase() + "'", []).then((data) => {
                                this.product_ids_for_filters = [];


                                if (data.rows.length > 0) {

                                    for (var s = 0; s < data.rows.length; s++) {

                                        this.product_ids_for_filters.push(
                                            data.rows.item(s).product_id
                                        );
                                    }

                                    this.getproduct_ids_for_filters.push(this.product_ids_for_filters);
                                    console.log("FINALLY " + JSON.stringify(this.getproduct_ids_for_filters));
                                    this.multi_intersect(this.getproduct_ids_for_filters);
                                }

                            }, (error) => {
                                console.log("ERROR:----" + JSON.stringify(error));
                            });


                        }
                        // console.log("FINALLYHuuuuu " + JSON.stringify(this.getproduct_ids_for_filters))
                        // this.multi_intersect(this.getproduct_ids_for_filters);
                    }


                }, (error) => {
                    console.log("ERROR:----" + JSON.stringify(error));
                });

            }
        }

        else {

            this.database.executeSql("SELECT * FROM " + this.GlobsalTableName, []).then((data) => {
                this.getProductData = [];
                this.product_ids = [];
                if (data.rows.length > 0) {
                    this.totalResult = data.rows.length;
                    for (var i = 0; i < data.rows.length; i++) {

                        /*  if(i==0)
                         console.log("IMAGEEEEE"+JSON.stringify(data.rows.item(i).cover_photo));*/

                        this.getProductData.push({
                            id: data.rows.item(i).id,
                            product_cat_id: data.rows.item(i).product_cat_id,
                            manufacturer_id: data.rows.item(i).manufacturer_id,
                            manufacturer_name: data.rows.item(i).manufacturer_name,
                            manufacturer_logo: data.rows.item(i).manufacturer_logo,
                            name: data.rows.item(i).name,
                            description: data.rows.item(i).description,
                            options: data.rows.item(i).options,
                            documentation: data.rows.item(i).documentation,
                            cover_photo: data.rows.item(i).cover_photo,
                            product_photo: JSON.parse(data.rows.item(i).product_photo),
                            options_id: JSON.parse(data.rows.item(i).options_id)

                        });
                        /* if(i==0)
                         console.log("IMAGEEEEE-DECODE"+JSON.stringify(atob(data.rows.item(i).cover_photo)));*/

                        // this.presentToast(JSON.parse(data.rows.item(i).product_photo));
                        this.product_ids.push({id: data.rows.item(i).id});

                        //  this.product_photo1.push({product_photo:data.rows.item(i).product_photo});

                        //  this.presentToast(tableName);
                    }

                    // this.getOptionData();
                }
            }, (error) => {
                console.log("ERROR:----" + JSON.stringify(error));
            });
        }

    }

    intersection(arr1, arr2) {
        var temp = [];

        for (var i in arr1) {
            var element = arr1[i];

            if (arr2.indexOf(element) > -1) {
                temp.push(element);
            }
        }

        return temp;
    }

    multi_intersect(array) {
        console.log("array" + array);
        var arrays = Array.prototype.slice.apply(array).slice(1);
        var temp = array[0];

        for (var i in arrays) {
            temp = this.intersection(arrays[i], temp);

            if (temp == []) {
                break;
            }
        }

        this.database.executeSql("SELECT * FROM " + this.GlobsalTableName + " where id IN (" + temp + ")", []).then((data) => {
            this.getProductData = [];
            this.totalResult = data.rows.length;
            for (var j = 0; j < data.rows.length; j++) {

                this.getProductData.push({
                    id: data.rows.item(j).id,
                    product_cat_id: data.rows.item(j).product_cat_id,
                    manufacturer_id: data.rows.item(j).manufacturer_id,
                    manufacturer_name: data.rows.item(j).manufacturer_name,
                    manufacturer_logo: data.rows.item(j).manufacturer_logo,
                    name: data.rows.item(j).name,
                    description: data.rows.item(j).description,
                    options: data.rows.item(j).options,
                    documentation: data.rows.item(j).documentation,
                    cover_photo: data.rows.item(j).cover_photo,
                    product_photo: JSON.parse(data.rows.item(j).product_photo),
                    options_id: JSON.parse(data.rows.item(j).options_id)

                });


            }

            console.log("ITSRESULT " + JSON.stringify(this.getProductData));

        }, (error) => {
            console.log("ERROR:----" + JSON.stringify(error));
        });

        console.log("XXXXXX" + temp);
        return temp;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad GlasstypePage');
    }

    back() {

        this.navCtrl.pop();
    }

    newMethod(name: String, icon: any, id: String, getProductData: Array<{id: string, product_cat_id: String, manufacturer_id: string, name: String, description: String, cover_photo: any}>) {

        console.log('Open Windows & Doors');
        this.menuCtrl.close();
        //this.navCtrl.push(WindowsPage)
        this.nav.push(GlasstypePage, {
            name: name, id: id, icon: icon, data: getProductData
        });
    }

    lableClick(id: String, key: String) {


        if (this.KeyForTable.indexOf(key) == -1) {
            this.KeyForTable.push(key);
        }


        if (this.allIDS.indexOf(id) == -1) {
            this.allIDS.push(id);

        } else {
            this.allIDS.splice(this.allIDS.indexOf(id), 1);
        }

        console.log(JSON.stringify(this.allIDS));


    }

    listViewClick(index: string) {
        let realName;
        if(this.em=="Manufacturers"){
             realName = this.getCatRealName[index].name;
        }
        else{
            realName = "";
        }



        this.sendProductData = this.getProductData[index].product_photo;
        this.optionData = this.getProductData[index].options_id;
        let name = this.getProductData[index].name;
        let description = this.getProductData[index].description;
        let options = this.getProductData[index].options;
        let documentation = this.getProductData[index].documentation;
        let manufacturer_logo = this.getProductData[index].manufacturer_logo;
        // let description = this.getProductData[index].description;


        console.log('detail page');
        this.menuCtrl.close();
        this.navCtrl.push(DetailpagePage, {
            name: name, description: description, data: JSON.stringify(this.sendProductData),
            options_id: JSON.stringify(this.optionData)
            , options: options, documentation: documentation, manufacturer_logo: manufacturer_logo,
            cat_name: this.em, cat_icon: this.icon,realName:realName

        })

    }
}
