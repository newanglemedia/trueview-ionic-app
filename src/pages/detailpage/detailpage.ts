import {Component} from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {HttpProvider} from "../../providers/http-provider";
import {SQLite} from "ionic-native";
import {GlasstypePage} from "../glasstype/glasstype";


/*
 Generated class for the Detailpage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    templateUrl: 'detailpage.html',
    providers: [HttpProvider]
})
export class DetailpagePage {

    public noWrapSlides:boolean = false;
    public myInterval: number = 1;
    public activeSlideIndex: number = 0;

    searchValue: String;
    getRelatedSearchData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String,options_id: String}>;
    public getCatData: Array<{name: String, icon: any, id: String}>;

    flag: boolean;

    name: string;
    cat_name: string;
    description: string;
    cat_icon: string;
    options: string;
    documentation: string;
    manufacturer_logo: string;
    public database: SQLite;
    getProductData_product_photo: Array<{product_photo: String}>;
    accordianData: Array<{name: String}>;
    getKey: Array<{key: String}>;
    //  getKey: any[];
    data: any[];
    options_id: any[];
    public isGroupOpen: boolean=false;
    public oneAtATime: boolean = true;
    options_flag: boolean = true;
    documentation_flag: boolean= true;

    /*   config: Object = {
     pagination: 'null',
     paginationClickable: false,x
     /!*nextButton: '.swiper-button-next',
     prevButton: '.swiper-button-prev',*!/
     spaceBetween: 0
     };


     */
    /* config: Object = {
     pagination: '.swiper-pagination',
     paginationClickable: true,
     nextButton: '.swiper-button-next',
     prevButton: '.swiper-button-prev',
     spaceBetween: 30
     };*/

    config: Object = {
        direction: 'horizontal',
        nextButton: '.swiper-button-next',
        pagination: '.swiper-pagination',
        prevButton: '.swiper-button-prev',
        slidesPerView: 'auto',
        interval:false,
        keyboardControl: true

    };

    constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController) {


        /* var mySwiper = new Swiper('.swiper-container', {
         speed: 400,
         spaceBetween: 100
         });*/
        this.flag = false;
        //this.isGroupOpen = false;
        this.name = navParams.get('name');
        this.cat_name = navParams.get('cat_name');
        this.cat_icon = navParams.get('cat_icon');
        this.description = navParams.get('description');
        this.options = navParams.get('options');
        this.documentation = navParams.get('documentation');
        this.manufacturer_logo = navParams.get('manufacturer_logo');
        this.data = JSON.parse(navParams.get('data'));
        this.options_id = JSON.parse(navParams.get('options_id'));

        if (this.cat_name == 'Manufacturers') {
            this.cat_name = navParams.get("realName");
        }


        this.getProductData_product_photo = [];
        this.getKey = [];
        this.accordianData = [];

      if( this.options==""){
          this.options_flag = false;
      }


        if( this.documentation==""){
            this.documentation_flag = false;
        }


        for (var key in this.options_id) {

            console.log("KEYE   " + key);
            // var obj = this.options_id[key];

            if(this.options_id[key].length!=0){
                this.getKey.push({key: key});
            }



        }


        for (var i = 0; i < this.data.length; i++) {
            //this.presentToast(this.documentation);

            try {
                this.getProductData_product_photo.push({product_photo: this.data[i]});
            } catch (ex) {

                console.log(ex.toString());

            }


        }

    }

    accordianClick(name: string) {

        this.accordianData = [];

        for (var i = 0; i < this.options_id[name].length; i++) {

            //this.presentToast(this.options_id[name].name);
            try {
                this.accordianData.push({name: this.options_id[name][i].name});
            } catch (ex) {

                console.log(ex.toString());

            }
        }
    }

    searchResult(name: String){


        this.getRelatedSearchData = [];
        for (var i = 0; i < this.getCatData.length; i++) {

            let cat_id = this.getCatData[i].id;

            if (cat_id != "0") {
                let cat_name;
                if (this.getCatData[i].name.match(' ')) {
                    let a = this.getCatData[i].name.indexOf(" ");
                    cat_name = this.getCatData[i].name.substring(0, a);
                }
                else {
                    cat_name = this.getCatData[i].name;
                }

                console.log("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"');

                this.database.executeSql("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"', []).then((data) => {


                    if (data.rows.length > 0) {

                        for (var i = 0; i < data.rows.length; i++) {


                            this.getRelatedSearchData.push({
                                id: data.rows.item(i).id,
                                product_cat_id: data.rows.item(i).product_cat_id,
                                manufacturer_id: data.rows.item(i).manufacturer_id,
                                manufacturer_name: data.rows.item(i).manufacturer_name,
                                manufacturer_logo: data.rows.item(i).manufacturer_logo,
                                name: data.rows.item(i).name,
                                description: data.rows.item(i).description,
                                options: data.rows.item(i).options,
                                documentation: data.rows.item(i).documentation,
                                cover_photo: data.rows.item(i).cover_photo,
                                product_photo: JSON.parse(data.rows.item(i).product_photo),
                                options_id: JSON.parse(data.rows.item(i).options_id)

                            });
                        }


                    }

                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });

            }

        }

        console.log('Open Windows & Doors');

        this.navCtrl.push(GlasstypePage, {
            name: 'Search for: ' + name, id: 'sdd', icon: 'dsfdsf', data: this.getRelatedSearchData
        });
    }

    showSearch(name: String) {

        // this.presentToast("sads","");

        if (this.name != ' ') {

            //  this.presentToast(name, "");
            this.flag = !this.flag;



            this.database = new SQLite();
            this.database .openDatabase({
                name: "data.db",
                location: "default"
            }).then(() => {
                this.database.executeSql("SELECT * FROM category", []).then((data) => {
                    this.getCatData = [];
                    if (data.rows.length > 0) {
                        // this.presentToast(data.rows.length, "");
                        for (var i = 0; i < data.rows.length; i++) {


                            // this.presentToast(name, "");

                            /*this.name = data.rows.item(i).name;
                            this.icon = data.rows.item(i).icon;
                            this.id = data.rows.item(i).id;*/


                            this.getCatData.push({name:data.rows.item(i).name, icon:data.rows.item(i).icon, id:data.rows.item(i).id});




                        }
                    }
                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });
            }, (error) => {
                console.error("Unable to execute sql", error);
            });

        }
        else {

            this.flag = !this.flag;
        }



    }

    presentToast(pd) {
        let toast = this.toastCtrl.create({
            message: pd,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');

        });

        toast.present();
    }

    back() {

        this.navCtrl.pop();
    }
}
