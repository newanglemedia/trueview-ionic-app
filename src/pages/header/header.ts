import {Component} from '@angular/core';
import {NavController, NavParams, MenuController, ToastController, Platform} from 'ionic-angular';
import {SQLite} from "ionic-native";
import {GlasstypePage} from "../glasstype/glasstype";
import {ManufacturersPage} from "../manufacturers/manufacturers";
import {DetailpagePage} from "../detailpage/detailpage";


/*
 Generated class for the Header page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-header',
    templateUrl: 'header.html',

})
export class HeaderPage {


    public database: SQLite;
    public getCatData: Array<{name: String, icon: any, id: String}>;
    name: String;
    filter_value: String;
    icon: any;
    id: String;
    searchValue: String;

    flag: boolean;
    getRelatedSearchData: Array<{id: string, product_cat_id: String, manufacturer_id: string, manufacturer_name: string, manufacturer_logo: string, name: String, description: String, options: String, documentation: String, cover_photo: any, product_photo: String, options_id: String}>;


    constructor(platform: Platform,public navCtrl: NavController, private toastCtrl: ToastController, private menuCtrl: MenuController, public menu: MenuController, public navParams: NavParams) {


        //this.refresh();
        this.flag = false;

        if(platform.is('tablet')){
           // this.presentToast("Tablet", "Tablet");

                //screen.msLockOrientation('portrait');
        }else if(platform.is('ipad')){
           // this.presentToast("phone", "phone");
        }
        else{
           // this.presentToast("phone", "phone");

        }


    }



    public refresh(name: String) {

        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {
            this.database.executeSql("SELECT * FROM category", []).then((data) => {
                this.getCatData = [];
                if (data.rows.length > 0) {
                    // this.presentToast(data.rows.length, "");
                    for (var i = 0; i < data.rows.length; i++) {

                        if (data.rows.item(i).name == name) {
                           // this.presentToast(name, "");

                            this.name = data.rows.item(i).name;
                            this.icon = data.rows.item(i).icon;
                            this.id = data.rows.item(i).id;

                            this.filter_value = data.rows.item(i).filter;


                            this.push(this.name, this.icon, this.id,this.filter_value);

                        }


                    }
                }
            }, (error) => {
                console.log("ERROR: " + JSON.stringify(error));
            });
        }, (error) => {
            console.error("Unable to execute sql", error);
        });
    }


    newMethod(name: String) {

        if (name == 'Manufacturers') {

            // this.presentToast("manufacture","");
            this.refresh(name);
        }

        else if (name == 'Windows and Doors') {
            this.refresh(name);
        }

        else if (name == 'Custom Showers') {

            // this.presentToast("custom_shower_design","");
            this.refresh(name);
        }
        else if (name == 'Service Products') {

            //  this.presentToast("glass_types","");
            this.refresh(name);
        }
        else if (name == 'Gallery') {
            /*this.navCtrl.push(DetailpagePage, {
				name: 'Product Gallery', description: description, data: JSON.stringify(this.sendProductData),
				options_id: JSON.stringify(this.optionData)
				, options: options, documentation: documentation, manufacturer_logo: manufacturer_logo,
				cat_name: this.em, cat_icon: this.icon,realName:realName
        	}); */
        this.database = new SQLite();
        this.database .openDatabase({
            name: "data.db",
            location: "default"
        }).then(() => {

            this.database.executeSql("SELECT * FROM Service WHERE id=146", []).then((data) => {
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {

                        var galData = {
                            id: data.rows.item(i).id,
                            product_cat_id: data.rows.item(i).product_cat_id,
                            manufacturer_id: data.rows.item(i).manufacturer_id,
                            manufacturer_name: data.rows.item(i).manufacturer_name,
                            manufacturer_logo: data.rows.item(i).manufacturer_logo,
                            name: data.rows.item(i).name,
                            description: data.rows.item(i).description,
                            options: data.rows.item(i).options,
                            documentation: data.rows.item(i).documentation,
                            cover_photo: data.rows.item(i).cover_photo,
                            product_photo: JSON.parse(data.rows.item(i).product_photo),
                            options_id: JSON.parse(data.rows.item(i).options_id)
                        };
						
						
						
						this.navCtrl.push(DetailpagePage, {
							name: galData.name, 
							description: galData.description, 
							data: JSON.stringify(galData.product_photo),
							options_id: galData.options_id, 
							options: galData.options, 
							documentation: galData.documentation, 
							manufacturer_logo: galData.manufacturer_logo,
							cat_name: 'Product Gallery', 
							cat_icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATZJREFUeNrsVtsNgkAQBEMBdOCVQAfSgZYAHWgFYgXagZSgFYgV0AIdaAc4mwzJSlAPufChTDIhd7sy95hd9LwJE/4CdV0n4EVx5VojUGKxml+Aelwhfnch6Pt+0d7hWEhEc0ZtM+JNmqejbqECUzU+qsXJkefgmeMluOq7+OBNTN9xyKcIbnBP+r4LHN8Ozz2YDBWW1W9bcycIphAJwYwGFFzBQxPj7q3LJ7MwhWFu2RErGTMW78m0uT6hwI4qOjLqiEcSkxzJtXmhrfDVwv2mletEeN7DsHOXwrFy9SvkHdUwWFhMs+YdpqxlT9V1Sg+se9WzpatvYMR8KamYDDkXMcepq5smUvKHoTR7NvymrkvVaAZ1rleQxrKFWDWkz38j7Ln4sATKkYuRvk759Fdrwm/iIcAAc1aUm1ZrrkYAAAAASUVORK5CYII=',
							realName: ''
						})
                    }
                   
                }
            }, (error) => {
                console.log("ERROR:----" + JSON.stringify(error));
            });

        }, (error) => {
            console.error("Unable to execute sql", error);
        });
        }
    }

    push(name: String, icon: any, id: String, filter_value:String) {

        if (name == 'Manufacturers') {


            console.log('Manufacturers');
            this.menuCtrl.close();
            //this.navCtrl.push(WindowsPage)
            this.navCtrl.push(ManufacturersPage, {
                name: name, id: id, icon: icon
            });
        }
        else {
            console.log('Open Windows & Doors');
            // this.menuCtrl.close();
            //this.navCtrl.push(WindowsPage)
            this.navCtrl.push(GlasstypePage, {
                name: name, id: id, icon: icon,filterValue: this.filter_value
            });
        }


    }

    searchResult(name: String){


        this.getRelatedSearchData = [];
        for (var i = 0; i < this.getCatData.length; i++) {

            let cat_id = this.getCatData[i].id;

            if (cat_id != "0") {
                let cat_name;
                if (this.getCatData[i].name.match(' ')) {
                    let a = this.getCatData[i].name.indexOf(" ");
                    cat_name = this.getCatData[i].name.substring(0, a);
                }
                else {
                    cat_name = this.getCatData[i].name;
                }

                console.log("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"');

                this.database.executeSql("SELECT * FROM " + cat_name + " WHERE name LIKE " + '"%' + name + '%" or manufacturer_name like "%' + name + '%"', []).then((data) => {


                    if (data.rows.length > 0) {

                        for (var i = 0; i < data.rows.length; i++) {


                            this.getRelatedSearchData.push({
                                id: data.rows.item(i).id,
                                product_cat_id: data.rows.item(i).product_cat_id,
                                manufacturer_id: data.rows.item(i).manufacturer_id,
                                manufacturer_name: data.rows.item(i).manufacturer_name,
                                manufacturer_logo: data.rows.item(i).manufacturer_logo,
                                name: data.rows.item(i).name,
                                description: data.rows.item(i).description,
                                options: data.rows.item(i).options,
                                documentation: data.rows.item(i).documentation,
                                cover_photo: data.rows.item(i).cover_photo,
                                product_photo: JSON.parse(data.rows.item(i).product_photo),
                                options_id: JSON.parse(data.rows.item(i).options_id)

                            });
                        }


                    }

                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });

            }

        }

        console.log('Open Windows & Doors');

        this.navCtrl.push(GlasstypePage, {
            name: 'Search for: ' + name, id: 'sdd', icon: 'dsfdsf', data: this.getRelatedSearchData
        });
    }

    showSearch(name: String) {

        // this.presentToast("sads","");

        if (this.name != ' ') {

          //  this.presentToast(name, "");
            this.flag = !this.flag;



            this.database = new SQLite();
            this.database .openDatabase({
                name: "data.db",
                location: "default"
            }).then(() => {
                this.database.executeSql("SELECT * FROM category", []).then((data) => {
                    this.getCatData = [];
                    if (data.rows.length > 0) {
                        // this.presentToast(data.rows.length, "");
                        for (var i = 0; i < data.rows.length; i++) {


                               // this.presentToast(name, "");

                                this.name = data.rows.item(i).name;
                                this.icon = data.rows.item(i).icon;
                                this.id = data.rows.item(i).id;


                                this.getCatData.push({name:this.name, icon:this.icon, id:this.id});




                        }
                    }
                }, (error) => {
                    console.log("ERROR: " + JSON.stringify(error));
                });
            }, (error) => {
                console.error("Unable to execute sql", error);
            });

        }
        else {

            this.flag = !this.flag;
        }



    }

    presentToast(em, pd) {
        let toast = this.toastCtrl.create({
            message: em + '' + pd,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');

        });

        toast.present();
    }

}
