import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public http: Http) {
    console.log('Hello HttpProvider Provider');
  }


   getJsonData(pd:String){

    return this.http.get('http://iskstaging.com/trueview/api/product_list?cat_id='+pd).map(res => res.json().Product);
    //return this.http.get('/api/products').map(res => res.json().Product);
       //http://iskstaging.com/trueview/api/products
       //https://www.reddit.com/r/gifs/top/.json?limit=10&sort=hot
       //https://randomuser.me/api/?results=10

  }

    getManufacturerData(){

        return this.http.get('http://iskstaging.com/trueview/api/manufacturer').map(res => res.json().Manufacturer);
        //return this.http.get('/api/products').map(res => res.json().Product);
        //http://iskstaging.com/trueview/api/products
        //https://www.reddit.com/r/gifs/top/.json?limit=10&sort=hot
        //https://randomuser.me/api/?results=10

    }

    getDynamicCategory(){

        return this.http.get('http://iskstaging.com/trueview/api/product_cat').map(res => res.json().ProductCat);
        //return this.http.get('/api/products').map(res => res.json().Product);
        //http://iskstaging.com/trueview/api/products
        //https://www.reddit.com/r/gifs/top/.json?limit=10&sort=hot
        //https://randomuser.me/api/?results=10

    }

    getDynamicData(){

        return this.http.get('http://trueviewonline.com/api/sync').map(res => res.json());
       // http://67.227.191.165/true_view/
        //return this.http.get('/api/products').map(res => res.json().Product);
        //http://iskstaging.com/trueview/api/products
        //https://www.reddit.com/r/gifs/top/.json?limit=10&sort=hot
        //https://randomuser.me/api/?results=10

    }

   // http://iskstaging.com/trueview/api/sync
}
